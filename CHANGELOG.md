## [4.0.1](https://gitlab.com/munipal-oss/fluctuate/compare/v4.0.0...v4.0.1) (2024-10-30)

### Bug Fixes

* retry 429 errors when using FQLv4 ([04b47a9](https://gitlab.com/munipal-oss/fluctuate/commit/04b47a9d5ecdc4c76724ca83ced6a05b7a2f2d1c))

## [4.0.0](https://gitlab.com/munipal-oss/fluctuate/compare/v3.0.0...v4.0.0) (2024-10-09)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pre-commit ^3.0.0

### Miscellaneous Chores

* **deps:** update dependency pre-commit to v4 ([373af6c](https://gitlab.com/munipal-oss/fluctuate/commit/373af6c51f610c158380d99efd693a5851c3e57f))

## [3.0.0](https://gitlab.com/munipal-oss/fluctuate/compare/v2.0.0...v3.0.0) (2024-05-30)

### ⚠ BREAKING CHANGES

* **deps:** drop support for fauna ^1.0.0

### Bug Fixes

* **deps:** update dependency fauna to v2 ([694addd](https://gitlab.com/munipal-oss/fluctuate/commit/694addde6904214f14739be5d626394fa441de92))

## [2.0.0](https://gitlab.com/munipal-oss/fluctuate/compare/v1.0.2...v2.0.0) (2023-08-22)


### ⚠ BREAKING CHANGES

* Drop Python 3.9 support as the FQLv10 driver doesn't
support 3.8

### Features

* add support for FQLv10 ([78d93db](https://gitlab.com/munipal-oss/fluctuate/commit/78d93db47e65a58b33071e1efac4f3dd12c89609))


### Bug Fixes

* **deps:** allow support for fauna beta releases up to 1.0.0 ([c59b0be](https://gitlab.com/munipal-oss/fluctuate/commit/c59b0be54d87383bd5a8530f5712d10e7c1f8ebe))
* **deps:** support fauna-python 1.x series now that FQLv10 is GA ([fec0d97](https://gitlab.com/munipal-oss/fluctuate/commit/fec0d97cf83a5703c05a306a0d4cc97b25aa4bae))
* resolve not handling null indexes when creating migrations collection ([1d02393](https://gitlab.com/munipal-oss/fluctuate/commit/1d02393208fe4f1ffa35e736235095ea254eb177))

## [1.0.2](https://gitlab.com/munipal-oss/fluctuate/compare/v1.0.1...v1.0.2) (2023-07-18)


### Bug Fixes

* **deps:** update dependency cloup to v3 ([bff876e](https://gitlab.com/munipal-oss/fluctuate/commit/bff876e26321e89da30780d3f36cfe053588a844))

## [1.0.1](https://gitlab.com/munipal-oss/fluctuate/compare/v1.0.0...v1.0.1) (2023-05-22)


### Bug Fixes

* remove deprecated specification of the domain in the faunadb client ([7cbb3a8](https://gitlab.com/munipal-oss/fluctuate/commit/7cbb3a894227cc6d1fab053de74b3de7ca9313f6))

## 1.0.0 (2023-05-06)


### Features

* initial release ([648ebcf](https://gitlab.com/munipal-oss/fluctuate/commit/648ebcf7a7b53408be50416a136db53448465605))

## 1.0.0-beta.1 (2023-05-06)


### Features

* initial release ([648ebcf](https://gitlab.com/munipal-oss/fluctuate/commit/648ebcf7a7b53408be50416a136db53448465605))
