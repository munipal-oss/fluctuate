from unittest.mock import ANY, MagicMock, call, create_autospec, patch

import pytest
from fauna import fql
from fauna.client import Client
from fauna.encoding import QuerySuccess
from fauna.errors import QueryRuntimeError
from fauna.query import Query
from faunadb import query
from faunadb.client import FaunaClient
from faunadb.errors import UnexpectedError
from faunadb.request_result import RequestResult

from fluctuate.migrations import (
    FqlVersion,
    Migration,
    _apply_migration,
    _apply_migrations,
    _build_scoped_key,
    _create_fauna_client,
    _delete_migration_document,
    _discover_migrations,
    _ensure_child_db,
    _filter_applied_migrations,
    _filter_migrations,
    _filter_to_migrations_up_to_target,
    _filter_unapplied_migrations,
    _find_migrations_module,
    _is_valid_migrations_module,
    _log_package_import_error,
    _retry_predicate,
    _target_in_migrations,
    _unapply_migration,
    _unapply_migrations,
    _write_migration_document,
    migrate,
    migrations_collection_name,
    migrations_index_name,
    unmigrate,
)


def test_migration___str__():
    """Test that converting a migration to a string works as expected."""
    migration = Migration(
        name="foo", namespace="bar", migration=None, reverse_migration=None
    )

    assert str(migration) == "bar.foo"


def test_migration_full_name():
    """Test that migration full name works as expected."""
    migration = Migration(
        name="foo", namespace="bar", migration=None, reverse_migration=None
    )

    assert migration.full_name == "bar.foo"


@patch(target="fluctuate.migrations._build_scoped_key", autospec=True)
@patch(target="fluctuate.migrations.FaunaClient", autospec=True)
@patch(target="fluctuate.migrations.Client", autospec=True)
def test__create_fauna_client_v4(
    mock_v10_client, mock_v4_client, mock__build_scoped_key
):
    """Test that creating the v4 fauna client works as expected."""
    mock_key = "test_key"

    result = _create_fauna_client(key=mock_key, fql_version=FqlVersion.v4)

    # Should have returned the instantiated FaunaDB client.
    assert result == mock_v4_client.return_value
    # Should have created the v4 client correctly.
    mock_v4_client.assert_called_once_with(secret=mock_key)
    # Should not have created a v10 client.
    mock_v10_client.assert_not_called()

    # Should not have created a scoped key.
    mock__build_scoped_key.assert_not_called()


@patch(target="fluctuate.migrations._build_scoped_key", autospec=True)
@patch(target="fluctuate.migrations.FaunaClient", autospec=True)
@patch(target="fluctuate.migrations.Client", autospec=True)
def test__create_fauna_client_v10(
    mock_v10_client, mock_v4_client, mock__build_scoped_key
):
    """Test that creating the v10 fauna client works as expected."""
    mock_key = "test_key"

    result = _create_fauna_client(key=mock_key, fql_version=FqlVersion.v10)

    # Should have returned the instantiated FaunaDB client.
    assert result == mock_v10_client.return_value
    # Should have created the v10 client correctly.
    mock_v10_client.assert_called_once_with(secret=mock_key, max_attempts=10)
    # Should not have created a v4 client.
    mock_v4_client.assert_not_called()

    # Should not have created a scoped key.
    mock__build_scoped_key.assert_not_called()


@patch(target="fluctuate.migrations._build_scoped_key", autospec=True)
@patch(target="fluctuate.migrations.FaunaClient", autospec=True)
@patch(target="fluctuate.migrations.Client", autospec=True)
def test__create_fauna_client_child_db_v4(
    mock_v10_client, mock_v4_client, mock__build_scoped_key
):
    """Test that creating the fauna v4 client works as expected when a child db is
    passed.
    """
    mock_key = "test_key"
    mock_child_db = "test_child_db"

    result = _create_fauna_client(
        key=mock_key, fql_version=FqlVersion.v4, child_db=mock_child_db
    )

    # Should have returned the instantiated FaunaDB client.
    assert result == mock_v4_client.return_value
    # Should have created the v4 client correctly.
    mock_v4_client.assert_called_once_with(secret=mock__build_scoped_key.return_value)

    # Should have created a scoped key.
    mock__build_scoped_key.assert_called_once_with(key=mock_key, child_db=mock_child_db)

    # Should not have created a v10 client.
    mock_v10_client.assert_not_called()


@patch(target="fluctuate.migrations._build_scoped_key", autospec=True)
@patch(target="fluctuate.migrations.FaunaClient", autospec=True)
@patch(target="fluctuate.migrations.Client", autospec=True)
def test__create_fauna_client_child_db_v10(
    mock_v10_client, mock_v4_client, mock__build_scoped_key
):
    """Test that creating the fauna v10 client works as expected when a child db is
    passed.
    """
    mock_key = "test_key"
    mock_child_db = "test_child_db"

    result = _create_fauna_client(
        key=mock_key, fql_version=FqlVersion.v10, child_db=mock_child_db
    )

    # Should have returned the instantiated FaunaDB client.
    assert result == mock_v10_client.return_value
    # Should have created the v10 client correctly.
    mock_v10_client.assert_called_once_with(
        secret=mock__build_scoped_key.return_value, max_attempts=10
    )

    # Should have created a scoped key.
    mock__build_scoped_key.assert_called_once_with(key=mock_key, child_db=mock_child_db)

    # Should not have created a v4 client.
    mock_v4_client.assert_not_called()


@patch(target="fluctuate.migrations._build_scoped_key", autospec=True)
@patch(target="fluctuate.migrations.FaunaClient", autospec=True)
@patch(target="fluctuate.migrations.Client", autospec=True)
def test__create_fauna_client_wrong_version(
    mock_v10_client, mock_v4_client, mock__build_scoped_key
):
    """Test that an error is raised when passing an invalid FQL version specifier."""
    mock_key = "test_key"

    with pytest.raises(ValueError, match=r"fql_version"):
        _create_fauna_client(key=mock_key, fql_version="invalid")

    # Should not have done anything.
    mock_v4_client.assert_not_called()
    mock_v10_client.assert_not_called()
    mock__build_scoped_key.assert_not_called()


@patch(target="fluctuate.migrations.fql", autospec=True)
def test__filter_migrations(mock_fql):
    """Test that filter migrations only returns the migrations matching the provided
    condition.
    """
    mock_fauna_v10_client = create_autospec(spec=Client, instance=True)
    mock_migrations = (
        Migration(
            name="bar",
            namespace="foo",
            migration=None,
            reverse_migration=None,
            child_database=None,
        ),
        Migration(
            name="baz",
            namespace="foo",
            migration=None,
            reverse_migration=None,
            child_database=None,
        ),
    )
    mock_condition = create_autospec(spec=fql, instance=True)
    # Set the filter to only return the first migration as a match.
    mock_fauna_v10_client.query.return_value.data = mock_migrations[0].full_name

    result = _filter_migrations(
        fauna_client=mock_fauna_v10_client,
        migrations=mock_migrations,
        condition=mock_condition,
    )

    # Should have returned the one matching migration.
    assert result == (mock_migrations[0],)

    # Should have filtered properly.
    expected_migration_objects = (
        {
            "full_name": mock_migrations[0].full_name,
            "name": mock_migrations[0].name,
            "namespace": mock_migrations[0].namespace,
            "child_database": mock_migrations[0].child_database,
        },
        {
            "full_name": mock_migrations[1].full_name,
            "name": mock_migrations[1].name,
            "namespace": mock_migrations[1].namespace,
            "child_database": mock_migrations[1].child_database,
        },
    )
    mock_fql.assert_called_once_with(
        ANY,
        migration_objects=expected_migration_objects,
        condition=mock_condition,
    )
    mock_fauna_v10_client.query.assert_called_once_with(mock_fql.return_value)


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations._filter_migrations", autospec=True)
def test__filter_unapplied_migrations(mock__filter_migrations, mock_fql):
    """Test that filter unapplied migrations filters to unapplied migrations."""
    mock_fauna_client = create_autospec(spec=Client, instance=True)
    mock_migrations = [
        create_autospec(
            spec=Migration(
                name=None,
                namespace=None,
                migration=None,
                reverse_migration=None,
                child_database=None,
            )
        ),
        create_autospec(
            spec=Migration(
                name=None,
                namespace=None,
                migration=None,
                reverse_migration=None,
                child_database=None,
            )
        ),
    ]

    result = _filter_unapplied_migrations(
        fauna_client=mock_fauna_client, migrations=mock_migrations
    )

    # Should have returned the filtered migrations.
    assert result == mock__filter_migrations.return_value

    # Should have filtered the migrations as expected.
    mock_fql.assert_called_once()
    mock__filter_migrations.assert_called_once_with(
        fauna_client=mock_fauna_client,
        migrations=mock_migrations,
        condition=mock_fql.return_value,
    )


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations._filter_migrations", autospec=True)
def test__filter_applied_migrations(mock__filter_migrations, mock_fql):
    """Test that filter unapplied migrations filters to unapplied migrations."""
    mock_fauna_client = create_autospec(spec=FaunaClient, instance=True)
    mock_migrations = [
        create_autospec(
            spec=Migration(
                name=None,
                namespace=None,
                migration=None,
                reverse_migration=None,
                child_database=None,
            )
        ),
        create_autospec(
            spec=Migration(
                name=None,
                namespace=None,
                migration=None,
                reverse_migration=None,
                child_database=None,
            )
        ),
    ]

    result = _filter_applied_migrations(
        fauna_client=mock_fauna_client, migrations=mock_migrations
    )

    # Should have returned the filtered migrations.
    assert result == mock__filter_migrations.return_value

    # Should have filtered the migrations as expected.
    mock_fql.assert_called_once()
    mock__filter_migrations.assert_called_once_with(
        fauna_client=mock_fauna_client,
        migrations=mock_migrations,
        condition=mock_fql.return_value,
    )


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__write_migration_document_v4(mock_query, mock_fql):
    """Test that the correct query to create the migration documents is returned when
    running a FQLv4 migration.
    """
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = "foo"
    mock_migration.namespace = "bar"
    mock_migration.migration = create_autospec(spec=query._Expr, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=query._Expr, instance=True)
    mock_migration.child_database = None

    expected_migration_data = {
        "name": mock_migration.name,
        "namespace": mock_migration.namespace,
        "child_database": mock_migration.child_database,
    }

    result = _write_migration_document(migration=mock_migration)

    # Should have returned the right query.
    assert result == mock_query.create.return_value
    # Should have built the query correctly.
    mock_query.collection.assert_called_once_with("fluctuate_migrations")
    mock_query.create.assert_called_once_with(
        mock_query.collection.return_value,
        {"data": expected_migration_data},
    )
    # Should not have used FQLv10.
    mock_fql.assert_not_called()


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__write_migration_document_v10(mock_query, mock_fql):
    """Test that the correct query to create the migration documents is returned when
    running a FQLv10 migration.
    """
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = "foo"
    mock_migration.namespace = "bar"
    mock_migration.migration = create_autospec(spec=Query, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=Query, instance=True)
    mock_migration.child_database = None

    expected_migration_data = {
        "name": mock_migration.name,
        "namespace": mock_migration.namespace,
        "child_database": mock_migration.child_database,
    }

    result = _write_migration_document(migration=mock_migration)

    # Should have returned the right query.
    assert result == mock_fql.return_value
    # Should have built the query correctly.
    mock_fql.assert_called_once_with(
        "fluctuate_migrations.create(${migration_data})",
        migration_data=expected_migration_data,
    )
    # Should not have used any v4 queries.
    mock_query.collection.assert_not_called()
    mock_query.create.assert_not_called()


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__delete_migration_document_v4(mock_query, mock_fql):
    """Test that the correct FQLv4 query to delete the migration document is returned."""
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = "foo"
    mock_migration.namespace = "bar"
    mock_migration.migration = create_autospec(spec=query._Expr, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=query._Expr, instance=True)
    mock_migration.child_database = None
    expected_migration_tuple = (mock_migration.name, mock_migration.namespace)

    result = _delete_migration_document(migration=mock_migration)

    # Should have returned the right query.
    assert result == mock_query.delete.return_value
    # Should have built the query correctly.
    mock_query.index.assert_called_once_with(migrations_index_name)
    mock_query.match.assert_called_once_with(
        mock_query.index.return_value, expected_migration_tuple
    )
    mock_query.get.assert_called_once_with(mock_query.match.return_value)
    mock_query.select.assert_called_once_with("ref", mock_query.get.return_value)
    mock_query.delete.assert_called_once_with(mock_query.select.return_value)
    # Should not have used any FQLv10.
    mock_fql.assert_not_called()


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__delete_migration_document_v10(mock_query, mock_fql):
    """Test that the correct FQLv10 query to delete the migration document is returned."""
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = "foo"
    mock_migration.namespace = "bar"
    mock_migration.migration = create_autospec(spec=Query, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=Query, instance=True)
    mock_migration.child_database = None

    result = _delete_migration_document(migration=mock_migration)

    # Should have returned the right query.
    assert result == mock_fql.return_value
    # Should have built the query correctly.
    mock_fql.assert_called_once_with(
        ANY,
        migration_name=mock_migration.name,
        migration_namespace=mock_migration.namespace,
    )
    # Should not have used any FQLv4.
    mock_query.index.assert_not_called()
    mock_query.match.assert_not_called()
    mock_query.get.assert_not_called()
    mock_query.select.assert_not_called()
    mock_query.delete.assert_not_called()


@patch(target="fluctuate.migrations.logger", autospec=True)
def test__log_package_import_error(mock_logger):
    """Ensure that when called it logs the module name."""
    _log_package_import_error("test")

    mock_logger.debug.assert_called_once_with(ANY, "test")


@patch(target="fluctuate.migrations.importlib", autospec=True)
@patch(target="fluctuate.migrations._log_package_import_error", autospec=True)
@patch(target="fluctuate.migrations.pkgutil", autospec=True)
@patch(target="fluctuate.migrations.Path", autospec=True)
def test__find_migrations_module(
    mock_path,
    mock_pkgutil,
    mock__log_package_import_error,
    mock_importlib,
):
    """Test that _find_migrations_module finds the migrations in the normal case."""
    # Set up the walk packages return value.
    mock_migrations_module_name = f"foo.bar.{migrations_collection_name}"
    mock_pkgutil.walk_packages.return_value = (
        (None, "foo", None),
        (None, mock_migrations_module_name, None),
    )

    result = _find_migrations_module()

    # Should have returned the imported module.
    assert result == mock_importlib.import_module.return_value
    # Should have tried to find the module in the current working directory.
    mock_path.cwd.assert_called_once_with()
    mock_pkgutil.walk_packages.assert_called_once_with(
        path=[str(mock_path.cwd.return_value)], onerror=mock__log_package_import_error
    )
    # Should have tried to import the module.
    mock_importlib.import_module.assert_called_once_with(
        name=mock_migrations_module_name
    )


@patch(target="fluctuate.migrations.importlib", autospec=True)
@patch(target="fluctuate.migrations._log_package_import_error", autospec=True)
@patch(target="fluctuate.migrations.pkgutil", autospec=True)
@patch(target="fluctuate.migrations.Path", autospec=True)
def test__find_migrations_module_no_match(
    mock_path,
    mock_pkgutil,
    mock__log_package_import_error,
    mock_importlib,
):
    """Test that _find_migrations_module returns `None` when no migrations module is
    found.
    """
    # Set up the walk packages return value.
    mock_pkgutil.walk_packages.return_value = (
        (None, "foo", None),
        (None, "bar", None),
    )

    result = _find_migrations_module()

    # Should have returned `None`.
    assert result is None
    # Should have tried to find the module in the current working directory.
    mock_path.cwd.assert_called_once_with()
    mock_pkgutil.walk_packages.assert_called_once_with(
        path=[str(mock_path.cwd.return_value)], onerror=mock__log_package_import_error
    )
    # Should not have tried to import the module.
    mock_importlib.import_module.assert_not_called()


@pytest.mark.parametrize("iterable_type", (tuple, list))
def test__is_valid_migrations_module(iterable_type):
    """Test that _is_valid_migrations_module returns `True` when all conditions for
    being a valid migrations module are met.
    """
    # Set up the mock module.
    mock_migrations_module = MagicMock()
    mock_migrations_module.__name__ = "test_module"
    mock_migration = create_autospec(
        spec=Migration(
            name=None, namespace=None, migration=None, reverse_migration=None
        )
    )
    mock_migrations_module.migrations = iterable_type((mock_migration,))

    result = _is_valid_migrations_module(migrations_module=mock_migrations_module)

    assert result


def test__is_valid_migrations_module_no_migrations_attribute():
    """Test that _is_valid_migrations_module returns `False` when the module has no
    attribute named "migrations".
    """
    # Set up the mock module.
    mock_migrations_module = MagicMock(spec=("__name__",))
    mock_migrations_module.__name__ = "test_module"
    del mock_migrations_module.migrations

    result = _is_valid_migrations_module(migrations_module=mock_migrations_module)

    assert not result


def test__is_valid_migrations_module_migrations_attribute_not_a_list_or_tuple():
    """Test that _is_valid_migrations_module returns `False` when the attribute named
    "migrations" is not a list or tuple.
    """
    # Set up the mock module.
    mock_migrations_module = MagicMock()
    mock_migrations_module.__name__ = "test_module"
    mock_migration = create_autospec(
        spec=Migration(
            name=None, namespace=None, migration=None, reverse_migration=None
        )
    )
    mock_migrations_module.migrations = mock_migration

    result = _is_valid_migrations_module(migrations_module=mock_migrations_module)

    assert not result


@pytest.mark.parametrize("iterable_type", (tuple, list))
def test__is_valid_migrations_module_migrations_attribute_empty(iterable_type):
    """Test that _is_valid_migrations_module returns `False` when the attribute named
    "migrations" is an empty list or tuple.
    """
    # Set up the mock module.
    mock_migrations_module = MagicMock()
    mock_migrations_module.__name__ = "test_module"
    mock_migrations_module.migrations = iterable_type()

    result = _is_valid_migrations_module(migrations_module=mock_migrations_module)

    assert not result


@pytest.mark.parametrize("iterable_type", (tuple, list))
def test__is_valid_migrations_module_migrations_attribute_not_all_migrations(
    iterable_type,
):
    """Test that _is_valid_migrations_module returns `False` when the attribute named
    "migrations" contains other types besides `Migration` instances.
    """
    # Set up the mock module.
    mock_migrations_module = MagicMock()
    mock_migrations_module.__name__ = "test_module"
    mock_migration = create_autospec(
        spec=Migration(
            name=None, namespace=None, migration=None, reverse_migration=None
        )
    )
    mock_migrations_module.migrations = iterable_type((mock_migration, "foo"))

    result = _is_valid_migrations_module(migrations_module=mock_migrations_module)

    assert not result


@patch(target="fluctuate.migrations._is_valid_migrations_module", autospec=True)
@patch(target="fluctuate.migrations._find_migrations_module", autospec=True)
def test__discover_migrations(
    mock__find_migrations_module, mock__is_valid_migrations_module
):
    """Test that _discover_migrations returns the migrations in the normal case."""
    mock__is_valid_migrations_module.return_value = True

    result = _discover_migrations()

    assert result == mock__find_migrations_module.return_value.migrations
    mock__find_migrations_module.assert_called_once_with()
    mock__is_valid_migrations_module.assert_called_once_with(
        migrations_module=mock__find_migrations_module.return_value
    )


@patch(target="fluctuate.migrations._is_valid_migrations_module", autospec=True)
@patch(target="fluctuate.migrations._find_migrations_module", autospec=True)
def test__discover_migrations_no_migrations_found(
    mock__find_migrations_module, mock__is_valid_migrations_module
):
    """Test that _discover_migrations returns an empty tuple if no migrations are found."""
    mock__find_migrations_module.return_value = None
    mock__is_valid_migrations_module.return_value = True

    result = _discover_migrations()

    assert result == tuple()
    mock__find_migrations_module.assert_called_once_with()
    mock__is_valid_migrations_module.assert_not_called()


@patch(target="fluctuate.migrations._is_valid_migrations_module", autospec=True)
@patch(target="fluctuate.migrations._find_migrations_module", autospec=True)
def test__discover_migrations_not_a_valid_migrations_module(
    mock__find_migrations_module, mock__is_valid_migrations_module
):
    """Test that _discover_migrations returns an empty tuple if the module is not a
    valid migrations module.
    """
    mock__is_valid_migrations_module.return_value = False

    result = _discover_migrations()

    assert result == tuple()
    mock__find_migrations_module.assert_called_once_with()
    mock__is_valid_migrations_module.assert_called_once_with(
        migrations_module=mock__find_migrations_module.return_value
    )


@pytest.mark.parametrize(
    "key, child_db, expected_result",
    (
        ("key1", "child_db_1", "key1:child_db_1:admin"),
        ("key2:server", "child_db_2", "key2:child_db_2:server"),
        (
            "key3:child_db_2:@doc/users/1234",
            "child_db_3",
            "key3:child_db_2/child_db_3:@doc/users/1234",
        ),
        (
            "key4:child_db_1/child_db_2/child_db_3:@role/leader",
            "child_db_4",
            "key4:child_db_1/child_db_2/child_db_3/child_db_4:@role/leader",
        ),
    ),
)
def test__build_scoped_key(key, child_db, expected_result):
    """Test that build scoped key returns the expected key structure based on a variety
    of possible inputs.
    """
    result = _build_scoped_key(key=key, child_db=child_db)

    assert result == expected_result


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
def test__ensure_child_db(mock__create_fauna_client, mock_fql):
    """Test that _ensure_child_db checks all child dbs for existence."""
    mock_key = "foo"
    mock_child_db = "child1/child2/child3"
    mock__create_fauna_client.return_value.query.return_value.data = True

    result = _ensure_child_db(key=mock_key, child_db=mock_child_db)

    # Should say all of the child DBs exist.
    assert result
    # Should create a client to log into each child database and check that it exists.
    mock__create_fauna_client.assert_has_calls(
        (
            call(key=mock_key, fql_version=FqlVersion.v10, child_db=None),
            call().query(mock_fql.return_value),
            call(key=mock_key, fql_version=FqlVersion.v10, child_db="child1"),
            call().query(mock_fql.return_value),
            call(key=mock_key, fql_version=FqlVersion.v10, child_db="child1/child2"),
            call().query(mock_fql.return_value),
        )
    )
    mock_fql.assert_has_calls(
        (
            call("Database.byName(${child_db_name}) != null", child_db_name="child1"),
            call("Database.byName(${child_db_name}) != null", child_db_name="child2"),
            call("Database.byName(${child_db_name}) != null", child_db_name="child3"),
        )
    )


@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
def test__ensure_child_db_missing(mock__create_fauna_client, mock_fql):
    """Test that _ensure_child_db checks all child dbs for existence and returns false
    if not all are existing.
    """
    mock_key = "foo"
    mock_child_db = "child1/child2/child3"
    # Set up a side effect such that the third child database doesn't exist.
    mock_query_result_1 = create_autospec(spec=QuerySuccess, instance=True)
    mock_query_result_1.data = True
    mock_query_result_2 = create_autospec(spec=QuerySuccess, instance=True)
    mock_query_result_2.data = True
    mock_query_result_3 = create_autospec(spec=QuerySuccess, instance=True)
    mock_query_result_3.data = False
    mock__create_fauna_client.return_value.query.side_effect = (
        mock_query_result_1,
        mock_query_result_2,
        mock_query_result_3,
    )

    result = _ensure_child_db(key=mock_key, child_db=mock_child_db)

    # Should say that not all of the child DBs exist.
    assert not result
    # Should create a client to log into each child database and check that it exists.
    mock__create_fauna_client.assert_has_calls(
        (
            call(key=mock_key, fql_version=FqlVersion.v10, child_db=None),
            call().query(mock_fql.return_value),
            call(key=mock_key, fql_version=FqlVersion.v10, child_db="child1"),
            call().query(mock_fql.return_value),
            call(key=mock_key, fql_version=FqlVersion.v10, child_db="child1/child2"),
            call().query(mock_fql.return_value),
        )
    )
    mock_fql.assert_has_calls(
        (
            call("Database.byName(${child_db_name}) != null", child_db_name="child1"),
            call("Database.byName(${child_db_name}) != null", child_db_name="child2"),
            call("Database.byName(${child_db_name}) != null", child_db_name="child3"),
        )
    )


def test__target_in_migrations():
    """Test that target in migrations finds the full name of the migration that matches
    the provided target.
    """
    migrations = (
        Migration(name="foo", namespace="bar", migration=None, reverse_migration=None),
        Migration(
            name="test_name",
            namespace="test_namespace",
            migration=None,
            reverse_migration=None,
        ),
    )
    target = "test_namespace.test_name"

    result = _target_in_migrations(target=target, migrations=migrations)

    assert result


def test__target_in_migrations_target_not_found():
    """Test that target in migrations returns `False` when the target migration is not
    found.
    """
    migrations = (
        Migration(name="foo", namespace="bar", migration=None, reverse_migration=None),
    )
    target = "test_namespace.test_name"

    result = _target_in_migrations(target=target, migrations=migrations)

    assert not result


def test__filter_to_migrations_up_to_target():
    """Test that _filter_to_migrations_up_to_target properly drops from the reversed
    list of migrations to only leave those to unapply up to the target.
    """
    reversed_migrations = (
        Migration(
            name="test4", namespace="test4", migration=None, reverse_migration=None
        ),
        Migration(
            name="test3", namespace="test3", migration=None, reverse_migration=None
        ),
        Migration(
            name="test2", namespace="test2", migration=None, reverse_migration=None
        ),
        Migration(
            name="test1", namespace="test1", migration=None, reverse_migration=None
        ),
    )

    result = _filter_to_migrations_up_to_target(
        target="test3.test3", migrations=reversed_migrations
    )

    # Should drop the last two in the reversed migrations order.
    assert result == reversed_migrations[0:2]


@patch(target="fluctuate.migrations._write_migration_document", autospec=True)
@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__apply_v4_migration(mock_query, mock_fql, mock__write_migration_document):
    """Test that apply migration runs the appropriate FQLv4 query to run the migration
    and to create the migration record.
    """
    mock_fauna_v4_client = create_autospec(spec=FaunaClient, instance=True)
    mock_fauna_v10_client = create_autospec(spec=Client, instance=True)
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = None
    mock_migration.namespace = None
    mock_migration.migration = create_autospec(spec=query._Expr, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=query._Expr, instance=True)

    _apply_migration(
        fauna_v4_client=mock_fauna_v4_client,
        fauna_v10_client=mock_fauna_v10_client,
        migration=mock_migration,
    )

    # Should have built the correct query.
    mock_query.do.assert_called_once_with(
        mock_migration.migration, mock__write_migration_document.return_value
    )
    mock__write_migration_document.assert_called_once_with(migration=mock_migration)
    # Should have executed it.
    mock_fauna_v4_client.query.assert_called_once_with(mock_query.do.return_value)
    # Should not have performed any FQLv10 actions.
    mock_fauna_v10_client.query.assert_not_called()
    mock_fql.assert_not_called()


@patch(target="fluctuate.migrations._write_migration_document", autospec=True)
@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__apply_v10_migration(mock_query, mock_fql, mock__write_migration_document):
    """Test that apply migration runs the appropriate FQLv10 query to run the migration
    and to create the migration record.
    """
    mock_fauna_v4_client = create_autospec(spec=FaunaClient, instance=True)
    mock_fauna_v10_client = create_autospec(spec=Client, instance=True)
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = None
    mock_migration.namespace = None
    mock_migration.migration = create_autospec(spec=Query, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=Query, instance=True)

    _apply_migration(
        fauna_v4_client=mock_fauna_v4_client,
        fauna_v10_client=mock_fauna_v10_client,
        migration=mock_migration,
    )

    # Should have built the correct query.
    mock_fql.assert_called_once_with(
        ANY,
        migration=mock_migration.migration,
        write_migration_document=mock__write_migration_document.return_value,
    )
    mock__write_migration_document.assert_called_once_with(migration=mock_migration)
    # Should have executed it.
    mock_fauna_v10_client.query.assert_called_once_with(mock_fql.return_value)
    # Should not have performed any FQLv4 actions.
    mock_fauna_v4_client.query.assert_not_called()
    mock_query.do.assert_not_called()


@pytest.mark.parametrize("mock_child_db", (MagicMock(), None))
@patch(target="fluctuate.migrations._create_migrations_unique_index", autospec=True)
@patch(target="fluctuate.migrations._create_migrations_collection", autospec=True)
@patch(target="fluctuate.migrations._apply_migration", autospec=True)
@patch(target="fluctuate.migrations._filter_unapplied_migrations", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
def test__apply_migrations(
    mock__create_fauna_client,
    mock__filter_unapplied_migrations,
    mock__apply_migration,
    mock__create_migrations_collection,
    mock__create_migrations_unique_index,
    mock_child_db,
):
    """Test that _apply_migrations tries to apply all the unapplied migrations in one
    transaction and records the results in the migrations collection.
    """
    mock_key = MagicMock()
    # This is not iterated on and is passed `_filter_unapplied_migrations` which we have
    # mocked, so a MagicMock works fine.
    mock_migrations = MagicMock()
    # Need to set up the return value that will actually be iterated.
    spec = Migration(
        name=None,
        namespace=None,
        migration=create_autospec(spec=query._Expr, instance=True),
        reverse_migration=create_autospec(spec=query._Expr, instance=True),
    )
    mock__filter_unapplied_migrations.return_value = (
        create_autospec(spec=spec),
        create_autospec(spec=spec),
    )

    _apply_migrations(key=mock_key, child_db=mock_child_db, migrations=mock_migrations)

    # Should have created the client for the key and child_db
    mock__create_fauna_client.assert_has_calls(
        (
            call(key=mock_key, fql_version=FqlVersion.v10, child_db=mock_child_db),
            call(key=mock_key, fql_version=FqlVersion.v4, child_db=mock_child_db),
        )
    )
    # Should have used the client to create the migration schema documents and do the
    # migrations.
    mock__create_fauna_client.return_value.query.assert_has_calls(
        (
            call(mock__create_migrations_collection),
            call(mock__create_migrations_unique_index),
        )
    )
    # Should have filtered the migrations to ones that were unapplied.
    mock__filter_unapplied_migrations.assert_called_once_with(
        fauna_client=mock__create_fauna_client.return_value, migrations=mock_migrations
    )
    # Should have applied the migrations and written the records of the migrations that
    # were applied.
    mock__apply_migration.assert_has_calls(
        (
            call(
                fauna_v4_client=mock__create_fauna_client.return_value,
                fauna_v10_client=mock__create_fauna_client.return_value,
                migration=mock__filter_unapplied_migrations.return_value[0],
            ),
            call(
                fauna_v4_client=mock__create_fauna_client.return_value,
                fauna_v10_client=mock__create_fauna_client.return_value,
                migration=mock__filter_unapplied_migrations.return_value[1],
            ),
        )
    )


@pytest.mark.parametrize("mock_child_db", (MagicMock(), None))
@patch(target="fluctuate.migrations._create_migrations_unique_index", autospec=True)
@patch(target="fluctuate.migrations._create_migrations_collection", autospec=True)
@patch(target="fluctuate.migrations._apply_migration", autospec=True)
@patch(target="fluctuate.migrations._filter_unapplied_migrations", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
def test__apply_migrations_no_unapplied_migrations(
    mock__create_fauna_client,
    mock__filter_unapplied_migrations,
    mock__apply_migration,
    mock__create_migrations_collection,
    mock__create_migrations_unique_index,
    mock_child_db,
):
    """Test that _apply_migrations doesn't apply migrations if they are already applied."""
    mock_key = MagicMock()
    # This is not iterated on and is passed `_filter_unapplied_migrations` which we have
    # mocked, so a MagicMock works fine.
    mock_migrations = MagicMock()
    # Need to set up the return value that will actually be iterated.
    mock__filter_unapplied_migrations.return_value = tuple()

    _apply_migrations(key=mock_key, child_db=mock_child_db, migrations=mock_migrations)

    # Should have created the client for the key and child_db
    mock__create_fauna_client.assert_has_calls(
        (
            call(key=mock_key, fql_version=FqlVersion.v10, child_db=mock_child_db),
            call(key=mock_key, fql_version=FqlVersion.v4, child_db=mock_child_db),
        )
    )
    # Should have used the client to create the migration schema documents.
    mock__create_fauna_client.return_value.query.assert_has_calls(
        (
            call(mock__create_migrations_collection),
            call(mock__create_migrations_unique_index),
        )
    )
    # Should have filtered the migrations to ones that were unapplied.
    mock__filter_unapplied_migrations.assert_called_once_with(
        fauna_client=mock__create_fauna_client.return_value, migrations=mock_migrations
    )
    # Should not have done anything else.
    mock__apply_migration.assert_not_called()


@patch(target="fluctuate.migrations._delete_migration_document", autospec=True)
@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__unapply_migration_v4(mock_query, mock_fql, mock__delete_migration_document):
    """Test that unapply migration runs the appropriate FQLv4 query to reverse the
    migration and to remove the migration record.
    """
    mock_fauna_v4_client = create_autospec(spec=FaunaClient, instance=True)
    mock_fauna_v10_client = create_autospec(spec=Client, instance=True)
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = None
    mock_migration.namespace = None
    mock_migration.migration = create_autospec(spec=query._Expr, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=query._Expr, instance=True)

    _unapply_migration(
        fauna_v4_client=mock_fauna_v4_client,
        fauna_v10_client=mock_fauna_v10_client,
        migration=mock_migration,
    )

    # Should have built the correct query.
    mock_query.do.assert_called_once_with(
        mock_migration.reverse_migration, mock__delete_migration_document.return_value
    )
    mock__delete_migration_document.assert_called_once_with(migration=mock_migration)
    # Should have executed it.
    mock_fauna_v4_client.query.assert_called_once_with(mock_query.do.return_value)
    # Should not have run the FQLv10 query.
    mock_fauna_v10_client.query.assert_not_called()
    mock_fql.assert_not_called()


@patch(target="fluctuate.migrations._delete_migration_document", autospec=True)
@patch(target="fluctuate.migrations.fql", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
def test__unapply_migration_v10(mock_query, mock_fql, mock__delete_migration_document):
    """Test that unapply migration runs the appropriate FQLv10 query to reverse the
    migration and to remove the migration record.
    """
    mock_fauna_v4_client = create_autospec(spec=FaunaClient, instance=True)
    mock_fauna_v10_client = create_autospec(spec=Client, instance=True)
    mock_migration = create_autospec(spec=Migration, instance=True)
    mock_migration.name = None
    mock_migration.namespace = None
    mock_migration.migration = create_autospec(spec=Query, instance=True)
    mock_migration.reverse_migration = create_autospec(spec=Query, instance=True)

    _unapply_migration(
        fauna_v4_client=mock_fauna_v4_client,
        fauna_v10_client=mock_fauna_v10_client,
        migration=mock_migration,
    )

    # Should have built the correct query.
    mock_fql.assert_called_once_with(
        ANY,
        reverse_migration=mock_migration.reverse_migration,
        delete_migration_document=mock__delete_migration_document.return_value,
    )
    mock__delete_migration_document.assert_called_once_with(migration=mock_migration)
    # Should have executed it.
    mock_fauna_v10_client.query.assert_called_once_with(mock_fql.return_value)
    # Should not have run the FQLv4 query.
    mock_query.do.assert_not_called()
    mock_fauna_v4_client.query.assert_not_called()


@pytest.mark.parametrize("mock_child_db", (MagicMock(), None))
@patch(target="fluctuate.migrations._create_migrations_unique_index", autospec=True)
@patch(target="fluctuate.migrations._create_migrations_collection", autospec=True)
@patch(target="fluctuate.migrations._unapply_migration", autospec=True)
@patch(target="fluctuate.migrations._filter_applied_migrations", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
@patch(target="fluctuate.migrations._ensure_child_db", autospec=True)
# pylint: disable=too-many-arguments
def test__unapply_migrations(
    mock__ensure_child_db,
    mock__create_fauna_client,
    mock__filter_applied_migrations,
    mock__unapply_migration,
    mock__create_migrations_collection,
    mock__create_migrations_unique_index,
    mock_child_db,
):
    """Test that _unapply_migrations unapplies migrations that are still applied."""
    mock_key = MagicMock()
    # Make it so the child dbs exist.
    mock__ensure_child_db.return_value = True
    # This is not iterated on and is passed `_filter_unapplied_migrations` which we have
    # mocked, so a MagicMock works fine.
    mock_migrations = MagicMock()
    # Need to set up the return value that will actually be iterated.
    spec = Migration(name=None, namespace=None, migration=None, reverse_migration=None)
    mock__filter_applied_migrations.return_value = (
        create_autospec(spec=spec),
        create_autospec(spec=spec),
    )

    _unapply_migrations(
        key=mock_key, child_db=mock_child_db, migrations=mock_migrations
    )

    # Should have checked that the child DB existed.
    if mock_child_db is not None:
        mock__ensure_child_db.assert_called_once_with(
            key=mock_key, child_db=mock_child_db
        )
    else:
        mock__ensure_child_db.assert_not_called()

    # Should have created the client for the key and child_db
    mock__create_fauna_client.assert_has_calls(
        (
            call(key=mock_key, fql_version=FqlVersion.v10, child_db=mock_child_db),
            call(key=mock_key, fql_version=FqlVersion.v4, child_db=mock_child_db),
        )
    )
    # Should have used the client to create the migration schema documents.
    mock__create_fauna_client.return_value.query.assert_has_calls(
        (
            call(mock__create_migrations_collection),
            call(mock__create_migrations_unique_index),
        )
    )
    # Should have filtered the migrations to ones that were applied.
    mock__filter_applied_migrations.assert_called_once_with(
        fauna_client=mock__create_fauna_client.return_value, migrations=mock_migrations
    )
    # Should have unapplied the migrations and deleted the records of the migrations
    # that were unapplied.
    mock__unapply_migration.assert_has_calls(
        (
            call(
                fauna_v4_client=mock__create_fauna_client.return_value,
                fauna_v10_client=mock__create_fauna_client.return_value,
                migration=mock__filter_applied_migrations.return_value[0],
            ),
            call(
                fauna_v4_client=mock__create_fauna_client.return_value,
                fauna_v10_client=mock__create_fauna_client.return_value,
                migration=mock__filter_applied_migrations.return_value[1],
            ),
        )
    )


@pytest.mark.parametrize("mock_child_db", (MagicMock(), None))
@patch(target="fluctuate.migrations._create_migrations_unique_index", autospec=True)
@patch(target="fluctuate.migrations._create_migrations_collection", autospec=True)
@patch(target="fluctuate.migrations._unapply_migration", autospec=True)
@patch(target="fluctuate.migrations._filter_applied_migrations", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
@patch(target="fluctuate.migrations._ensure_child_db", autospec=True)
# pylint: disable=too-many-arguments
def test__unapply_migrations_no_applied_migrations(
    mock__ensure_child_db,
    mock__create_fauna_client,
    mock__filter_applied_migrations,
    mock__unapply_migration,
    mock__create_migrations_collection,
    mock__create_migrations_unique_index,
    mock_child_db,
):
    """Test that _unapply_migrations doesn't try to unapply migrations that are already
    unapplied.
    """
    mock_key = MagicMock()
    # Make it so the child dbs exist.
    mock__ensure_child_db.return_value = True
    # This is not iterated on and is passed `_filter_unapplied_migrations` which we have
    # mocked, so a MagicMock works fine.
    mock_migrations = MagicMock()
    # Need to set up the return value that will actually be iterated.
    mock__filter_applied_migrations.return_value = tuple()

    _unapply_migrations(
        key=mock_key, child_db=mock_child_db, migrations=mock_migrations
    )

    # Should have checked that the child DB existed.
    if mock_child_db is not None:
        mock__ensure_child_db.assert_called_once_with(
            key=mock_key, child_db=mock_child_db
        )
    else:
        mock__ensure_child_db.assert_not_called()

    # Should have created the client for the key and child_db
    mock__create_fauna_client.assert_has_calls(
        (
            call(key=mock_key, fql_version=FqlVersion.v10, child_db=mock_child_db),
            call(key=mock_key, fql_version=FqlVersion.v4, child_db=mock_child_db),
        )
    )
    # Should have used the client to create the migration schema documents.
    mock__create_fauna_client.return_value.query.assert_has_calls(
        (
            call(mock__create_migrations_collection),
            call(mock__create_migrations_unique_index),
        )
    )
    # Should have filtered the migrations to ones that were applied.
    mock__filter_applied_migrations.assert_called_once_with(
        fauna_client=mock__create_fauna_client.return_value, migrations=mock_migrations
    )
    # Should have not done anything else.
    mock__unapply_migration.assert_not_called()


@patch(target="fluctuate.migrations._create_migrations_unique_index", autospec=True)
@patch(target="fluctuate.migrations._create_migrations_collection", autospec=True)
@patch(target="fluctuate.migrations._delete_migration_document", autospec=True)
@patch(target="fluctuate.migrations.query", autospec=True)
@patch(target="fluctuate.migrations._filter_applied_migrations", autospec=True)
@patch(target="fluctuate.migrations._create_fauna_client", autospec=True)
@patch(target="fluctuate.migrations._ensure_child_db", autospec=True)
# pylint: disable=too-many-arguments
def test__unapply_migrations_child_db_does_not_exist(
    mock__ensure_child_db,
    mock__create_fauna_client,
    mock__filter_applied_migrations,
    mock_query,
    mock__delete_migration_document,
    mock__create_migrations_collection,
    mock__create_migrations_unique_index,
):
    """Test that _unapply_migrations doesn't try to unapply migrations from a child DB
    that doesn't exist.
    """
    mock_key = MagicMock()
    mock_child_db = MagicMock()
    # Make it so the child dbs do not exist.
    mock__ensure_child_db.return_value = False
    # This is not iterated on and is passed `_filter_unapplied_migrations` which we have
    # mocked, so a MagicMock works fine.
    mock_migrations = MagicMock()
    # Need to set up the return value that will actually be iterated.
    mock__filter_applied_migrations.return_value = tuple()

    _unapply_migrations(
        key=mock_key, child_db=mock_child_db, migrations=mock_migrations
    )

    # Should have checked that the child DB existed.
    mock__ensure_child_db.assert_called_once_with(key=mock_key, child_db=mock_child_db)

    # Should not have done anything else.
    mock__create_fauna_client.assert_not_called()
    mock__create_fauna_client.return_value.query.assert_not_called()
    mock__filter_applied_migrations.assert_not_called()
    mock_query.do.assert_not_called()
    mock__delete_migration_document.assert_not_called()


@patch(target="fluctuate.migrations._apply_migrations", autospec=True)
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_migrate(mock__discover_migrations, mock__apply_migrations):
    """Test that migrate applies all discovered migrations."""
    mock_key = MagicMock()
    # Set up a mock tuple of discovered migrations, with two different groups based on
    # child DB.
    spec = Migration(name=None, namespace=None, migration=None, reverse_migration=None)
    migration_1 = create_autospec(spec=spec)
    migration_1.child_database = None
    migration_2 = create_autospec(spec=spec)
    migration_2.child_database = None
    migration_3 = create_autospec(spec=spec)
    migration_3.child_database = "child_db"
    migration_4 = create_autospec(spec=spec)
    migration_4.child_database = "child_db"
    mock__discover_migrations.return_value = (
        migration_1,
        migration_2,
        migration_3,
        migration_4,
    )

    migrate(key=mock_key)

    # Should have discovered migrations.
    mock__discover_migrations.assert_called_once_with()
    # Should have applied migrations for the two different groups separately.
    mock__apply_migrations.assert_has_calls(
        (
            call(
                key=mock_key,
                child_db=None,
                migrations=mock__discover_migrations.return_value[0:2],
            ),
            call(
                key=mock_key,
                child_db="child_db",
                migrations=mock__discover_migrations.return_value[2:],
            ),
        )
    )


@patch(target="fluctuate.migrations._apply_migrations", autospec=True)
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_migrate_no_discovered_migrations(
    mock__discover_migrations, mock__apply_migrations
):
    """Test that migrate does nothing if no migrations are discovered.."""
    mock_key = MagicMock()
    # Set up a mock tuple of discovered migrations to be empty.
    mock__discover_migrations.return_value = tuple()

    migrate(key=mock_key)

    # Should have discovered migrations.
    mock__discover_migrations.assert_called_once_with()
    # Should not have applied migrations.
    mock__apply_migrations.assert_not_called()


@patch(target="fluctuate.migrations._unapply_migrations", autospec=True)
@patch(target="fluctuate.migrations._filter_to_migrations_up_to_target", autospec=True)
@patch(target="fluctuate.migrations._target_in_migrations", autospec=True)
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_unmigrate(
    mock__discover_migrations,
    mock__target_in_migrations,
    mock__filter_to_migrations_up_to_target,
    mock__unapply_migrations,
):
    """Test that unmigrate removes all applied migrations when no target is provided."""
    mock_key = MagicMock()
    # Set up a mock tuple of discovered migrations, with two different groups based on
    # child DB.
    spec = Migration(name=None, namespace=None, migration=None, reverse_migration=None)
    migration_1 = create_autospec(spec=spec)
    migration_1.child_database = None
    migration_2 = create_autospec(spec=spec)
    migration_2.child_database = None
    migration_3 = create_autospec(spec=spec)
    migration_3.child_database = "child_db"
    migration_4 = create_autospec(spec=spec)
    migration_4.child_database = "child_db"
    mock__discover_migrations.return_value = (
        migration_1,
        migration_2,
        migration_3,
        migration_4,
    )

    unmigrate(key=mock_key)

    # Should have discovered migrations.
    mock__discover_migrations.assert_called_once_with()
    # Should have unapplied migrations for the two different groups separately. They
    # should also be unapplied in reverse order.
    mock__unapply_migrations.assert_has_calls(
        (
            call(
                key=mock_key,
                child_db="child_db",
                migrations=tuple(reversed(mock__discover_migrations.return_value[2:])),
            ),
            call(
                key=mock_key,
                child_db=None,
                migrations=tuple(reversed(mock__discover_migrations.return_value[0:2])),
            ),
        )
    )
    # Should not have done anything with targets since no target was specified.
    mock__target_in_migrations.assert_not_called()
    mock__filter_to_migrations_up_to_target.assert_not_called()


@patch(target="fluctuate.migrations._unapply_migrations", autospec=True)
@patch(target="fluctuate.migrations._filter_to_migrations_up_to_target", autospec=True)
@patch(target="fluctuate.migrations._target_in_migrations", autospec=True)
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_unmigrate_no_applied_migrations(
    mock__discover_migrations,
    mock__target_in_migrations,
    mock__filter_to_migrations_up_to_target,
    mock__unapply_migrations,
):
    """Test that unmigrate does not try to unapply migrations that are already
    unapplied.
    """
    mock_key = MagicMock()
    # Set up a mock tuple of discovered migrations that is empty.
    mock__discover_migrations.return_value = tuple()

    unmigrate(key=mock_key)

    # Should have discovered migrations.
    mock__discover_migrations.assert_called_once_with()
    # Should not have done anything else.
    mock__unapply_migrations.assert_not_called()
    # Should not have done anything with targets since no target was specified.
    mock__target_in_migrations.assert_not_called()
    mock__filter_to_migrations_up_to_target.assert_not_called()


@patch(target="fluctuate.migrations._unapply_migrations", autospec=True)
@patch(target="fluctuate.migrations._filter_to_migrations_up_to_target", autospec=True)
@patch(target="fluctuate.migrations._target_in_migrations", autospec=True)
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_unmigrate_target(
    mock__discover_migrations,
    mock__target_in_migrations,
    mock__filter_to_migrations_up_to_target,
    mock__unapply_migrations,
):
    """Test that unmigrate only unapplies up to the target migration."""
    mock_key = MagicMock()
    mock_target = MagicMock()
    # Set up a mock tuple of discovered migrations, with two different groups based on
    # child DB.
    spec = Migration(name=None, namespace=None, migration=None, reverse_migration=None)
    migration_1 = create_autospec(spec=spec)
    migration_1.child_database = None
    migration_2 = create_autospec(spec=spec)
    migration_2.child_database = None
    migration_3 = create_autospec(spec=spec)
    migration_3.child_database = "child_db"
    migration_4 = create_autospec(spec=spec)
    migration_4.child_database = "child_db"
    mock__discover_migrations.return_value = (
        migration_1,
        migration_2,
        migration_3,
        migration_4,
    )
    # Make sure the target is in the migrations.
    mock__target_in_migrations.return_value = True

    unmigrate(key=mock_key, target=mock_target)

    # Should have discovered migrations.
    mock__discover_migrations.assert_called_once_with()
    # Should have checked the target before starting the loop and in the loop.
    mock__target_in_migrations.assert_has_calls(
        (
            call(target=mock_target, migrations=mock__discover_migrations.return_value),
            call(
                target=mock_target,
                migrations=tuple(reversed(mock__discover_migrations.return_value[2:])),
            ),
        )
    )
    # Should have filtered migrations based on the target.
    mock__filter_to_migrations_up_to_target.assert_called_once_with(
        target=mock_target,
        migrations=tuple(reversed(mock__discover_migrations.return_value[2:])),
    )
    # Should have unapplied migrations up to the target.
    mock__unapply_migrations.assert_called_once_with(
        key=mock_key,
        child_db="child_db",
        migrations=mock__filter_to_migrations_up_to_target.return_value,
    )


@patch(target="fluctuate.migrations._unapply_migrations", autospec=True)
@patch(target="fluctuate.migrations._filter_to_migrations_up_to_target", autospec=True)
@patch(target="fluctuate.migrations._target_in_migrations", autospec=True)
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_unmigrate_target_not_in_discovered_migrations(
    mock__discover_migrations,
    mock__target_in_migrations,
    mock__filter_to_migrations_up_to_target,
    mock__unapply_migrations,
):
    """Test that unmigrate raises an error if the provided target is not found in the
    discovered migrations.
    """
    mock_key = MagicMock()
    mock_target = MagicMock()
    # Set up a mock tuple of discovered migrations, with two different groups based on
    # child DB.
    spec = Migration(name=None, namespace=None, migration=None, reverse_migration=None)
    migration_1 = create_autospec(spec=spec)
    migration_1.child_database = None
    migration_2 = create_autospec(spec=spec)
    migration_2.child_database = None
    migration_3 = create_autospec(spec=spec)
    migration_3.child_database = "child_db"
    migration_4 = create_autospec(spec=spec)
    migration_4.child_database = "child_db"
    mock__discover_migrations.return_value = (
        migration_1,
        migration_2,
        migration_3,
        migration_4,
    )
    # Make sure the target is not in the migrations.
    mock__target_in_migrations.return_value = False

    with pytest.raises(ValueError, match=rf"{mock_target}"):
        unmigrate(key=mock_key, target=mock_target)

    # Should have discovered migrations.
    mock__discover_migrations.assert_called_once_with()
    # Should have checked the target before starting the loop.
    mock__target_in_migrations.assert_called_once_with(
        target=mock_target, migrations=mock__discover_migrations.return_value
    )
    # Should not have done anything else.
    mock__filter_to_migrations_up_to_target.assert_not_called()
    mock__unapply_migrations.assert_not_called()


@pytest.mark.integration
@patch(target="fluctuate.migrations._discover_migrations", autospec=True)
def test_migrate_unique_constraint(
    mock__discover_migrations, test_db, test_db_scoped_key, fauna_admin_key
):
    """Test that migrate fails if a unique constraint is violated on the migration name
    and namespace.
    """
    scoped_key = test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    # Set up a mock tuple of discovered migrations with conflicting names.
    mock__discover_migrations.return_value = (
        Migration(
            name="my_broken_migration",
            namespace="test_migrate_unique_constraint",
            migration=fql(
                """
            Collection.create({name: "broken_migration_collection", history_days: 0})
            """
            ),
            reverse_migration=fql(
                'Collection.byName("broken_migration_collection").delete()'
            ),
            child_database=None,
        ),
        Migration(
            name="my_broken_migration",
            namespace="test_migrate_unique_constraint",
            migration=fql(
                """
            Collection.create({name: "broken_migration_collection", history_days: 0})
            """
            ),
            reverse_migration=fql(
                'Collection.byName("broken_migration_collection").delete()'
            ),
            child_database=None,
        ),
    )

    with pytest.raises(QueryRuntimeError) as exc_info:
        migrate(key=scoped_key)

    # Ensure it was a constraint error.
    assert exc_info.value.code == "constraint_failure"


def test__retry_predicate_has_attribute_and_429_error():
    """Test that the retry predicate works as expected when given a FaunaException."""
    # Set up the mock objects.
    mock_exception = create_autospec(spec=UnexpectedError, instance=True)
    mock_request_response = create_autospec(spec=RequestResult, instance=True)
    mock_request_response.status_code = 429
    mock_exception.request_result = mock_request_response

    # Call the retry predicate with the mock exception, and assert it returns that we
    # should retry.
    assert _retry_predicate(mock_exception)


def test__retry_predicate_doesnt_have_attribute():
    """Test that the retry predicate works as expected when given a different exception."""
    # Set up the mock objects.
    mock_exception = ValueError("test")

    # Call the retry predicate with the mock exception, and assert it returns that we
    # should not retry.
    assert not _retry_predicate(mock_exception)


def test__retry_predicate_has_attribute_doesnt_have_429_error():
    """Test that the retry predicate works as expected when given a FaunaException, but
    it doesn't contain a 429 error.
    """
    # Set up the mock objects.
    mock_exception = create_autospec(spec=UnexpectedError, instance=True)
    mock_request_response = create_autospec(spec=RequestResult, instance=True)
    mock_request_response.status_code = 400
    mock_exception.request_result = mock_request_response

    # Call the retry predicate with the mock exception, and assert it returns that we
    # should not retry.
    assert not _retry_predicate(mock_exception)
