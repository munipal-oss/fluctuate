import json
from uuid import uuid4

import boto3
import pytest
from decouple import config
from fauna import fql
from fauna.client import Client
from fauna.encoding import FaunaDecoder
from faunadb._json import parse_json
from faunadb.client import FaunaClient


@pytest.fixture(scope="session")
def secretsmanager_client():
    """Return a secrets manager client for use in the test.

    Usage of this fixture requires that the credentials be configured for boto3 via one
    of the following documented methods that does not involve setting the credentials on
    the client or session objects:
    https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html

    If credentials are not found, or any other configuration error occurs, the test
    requesting this fixture is skipped.

    This fixture is session scoped as the AWS credentials are not expected to change
    during the session, so we can save time by not needing to re-instantiate the
    SecretsManager client multiple times.
    """
    return boto3.client("secretsmanager")


@pytest.fixture(scope="session")
def fauna_admin_key(secretsmanager_client):
    """Attempts to return a Fauna DB admin key based on the environment configuration.

    In order to use this fixture, one of `FAUNA_ADMIN_KEY` or
    `FAUNA_ADMIN_KEY_SECRET_ARN` must either be set as an environment variable or in a
    .env file. If neither are set, the test requesting this fixture is skipped.

    `FAUNA_ADMIN_KEY` takes precedent over `FAUNA_ADMIN_KEY_SECRET_ARN`.

    This fixture is session scoped to reduce potentially hitting SecretsManager multiple
    times to retrieve the same secret value in the same test session.
    """
    fauna_admin_key = config("FAUNA_ADMIN_KEY", default=None)
    fauna_secret_arn = config("FAUNA_ADMIN_KEY_SECRET_ARN", default=None)
    if fauna_admin_key is None and fauna_secret_arn is None:
        pytest.fail(
            "Cannot access FaunaDB without setting `FAUNA_ADMIN_KEY` or"
            " `FAUNA_ADMIN_KEY_SECRET_ARN` in an environment variable or .env file."
        )

    if fauna_admin_key is not None:
        return fauna_admin_key

    secret = secretsmanager_client.get_secret_value(SecretId=fauna_secret_arn)
    # Parse the result into a FaunaDB Key object.
    try:
        secret = FaunaDecoder.decode(json.loads(secret["SecretString"]))
    except Exception:
        secret = parse_json(secret["SecretString"])

    # Return the secret key out of the Key object.
    return secret["secret"]


@pytest.fixture(scope="session")
def fauna_v10_admin_client(fauna_admin_key):
    """Create an return a FQLv10 admin client for the top level database.

    This fixture is session scoped as the admin key is expected to be set once and not
    changed during the session, so we can save time by not needing to re-instantiate the
    admin client multiple times.
    """
    return Client(secret=fauna_admin_key, max_attempts=10)


@pytest.fixture(scope="session")
def fauna_v4_admin_client(fauna_admin_key):
    """Create an return a FQLv4 admin client for the top level database.

    This fixture is session scoped as the admin key is expected to be set once and not
    changed during the session, so we can save time by not needing to re-instantiate the
    admin client multiple times.
    """
    return FaunaClient(secret=fauna_admin_key)


@pytest.fixture
def test_db_scoped_key():
    """This fixture returns a method to use to construct a scoped key for the provided
    child DB.

    See the following documentation link for more info on scoped keys:
    https://docs.fauna.com/fauna/current/security/keys#scoped-keys
    """

    def _inner(test_db, admin_key):
        """This returns a key scoped to the child DB given the child DB object from
        FaunaDB and the admin key for the parent DB.

        See the following documentation link for more info on scoped keys:
        https://docs.fauna.com/fauna/current/security/keys#scoped-keys
        """
        return f"{admin_key}:{test_db['name']}:admin"

    return _inner


@pytest.fixture
def test_db(fauna_v10_admin_client):
    """Create a randomly named test child database for use in this test module and
    return its name.

    This will delete the test database after the session completes.
    """
    # Create the test database
    test_db_name = f"test_fluctuate_{uuid4().hex}"
    result = fauna_v10_admin_client.query(
        fql(
            "Database.create({name: ${test_db_name}}) {name}", test_db_name=test_db_name
        )
    )

    # Yield the test database.
    yield result.data

    # Use a top level admin key to delete the child database.
    fauna_v10_admin_client.query(
        fql("Database.byName(${test_db_name}).delete()", test_db_name=test_db_name)
    )


@pytest.fixture
def fauna_v10_test_client(test_db, test_db_scoped_key, fauna_admin_key):
    """Returns a FQLv10 test client configured with access to a test fauna database."""
    # Create a fauna client using a scoped key to the child database. See the following
    # documentation link for more info on scoped keys:
    # https://docs.fauna.com/fauna/current/security/keys#scoped-keys
    return Client(
        secret=test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key),
        max_attempts=10,
    )


@pytest.fixture
def fauna_v4_test_client(test_db, test_db_scoped_key, fauna_admin_key):
    """Returns a FQLv4 test client configured with access to a test fauna database."""
    # Create a fauna client using a scoped key to the child database. See the following
    # documentation link for more info on scoped keys:
    # https://docs.fauna.com/fauna/current/security/keys#scoped-keys
    return FaunaClient(
        secret=test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    )
