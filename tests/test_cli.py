import subprocess
from importlib.metadata import version
from unittest.mock import MagicMock, patch

import pytest
from fauna import fql
from fauna.client import Client
from faunadb import query
from faunadb.client import FaunaClient

from fluctuate.cli import _process_common_options
from fluctuate.migrations import (
    _build_scoped_key,
    _run_fql_v4_query_with_retry,
    migrations_collection_name,
    migrations_index_name,
)
from tests.cli_migrations.fluctuate_migrations import migrations


@pytest.mark.integration
def test_migrate_cli(
    fauna_v10_test_client,
    fauna_v4_test_client,
    test_db,
    test_db_scoped_key,
    fauna_admin_key,
):
    """Test the migrate command against a test database and ensure the test migrations
    are applied.
    """
    scoped_key = test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    # Run the command.
    subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "migrate", "--key", scoped_key],
        check=True,
    )

    # Check that all migration documents and schema documents were written as expected.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database is None
            ),
        )
    ).data

    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that the migrations themselves were actually applied.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_1") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_2") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_1") != null')
    ).data

    # Need to log into the child database and check those migrations.
    fauna_v10_test_client = Client(
        secret=_build_scoped_key(key=scoped_key, child_db="test_database_1"),
        max_attempts=10,
    )
    fauna_v4_test_client = FaunaClient(
        secret=_build_scoped_key(key=scoped_key, child_db="test_database_1")
    )
    # Check that all migration documents and schema documents were written as expected.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database == "test_database_1"
            ),
        )
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that the migrations themselves were actually applied.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_3") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_2") != null')
    ).data

    # Need to log into the grandchild database and check those migrations.
    fauna_v10_test_client = Client(
        secret=_build_scoped_key(
            key=scoped_key, child_db="test_database_1/test_database_2"
        ),
        max_attempts=10,
    )
    fauna_v4_test_client = FaunaClient(
        secret=_build_scoped_key(
            key=scoped_key, child_db="test_database_1/test_database_2"
        )
    )
    # Check that all migration documents and schema documents were written as expected.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database == "test_database_1/test_database_2"
            ),
        )
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that the migrations themselves were actually applied.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_4") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_5") != null')
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index("test_collection_4_index")),
    )


@pytest.mark.integration
def test_unmigrate_cli(
    fauna_v10_test_client,
    fauna_v4_test_client,
    test_db,
    test_db_scoped_key,
    fauna_admin_key,
):
    """Test the unimgrate command against a test database and ensure the test migrations
    are unapplied.
    """
    scoped_key = test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    # Run the migrate command.
    subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "migrate", "--key", scoped_key],
        check=True,
    )

    # Now remove the migrations.
    subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "unmigrate", "--key", scoped_key],
        check=True,
    )

    # Check that all migration documents were removed but schema documents remain.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(f"{migrations_collection_name}.all().isEmpty()")
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_1") == null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_2") == null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_1") == null')
    ).data


@pytest.mark.integration
def test_unmigrate_target_cli(
    fauna_v10_test_client,
    fauna_v4_test_client,
    test_db,
    test_db_scoped_key,
    fauna_admin_key,
):
    """Test the unimgrate command against a test database and ensure the test migrations
    are unapplied up to the target migration.
    """
    scoped_key = test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    # Run the migrate command.
    subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "migrate", "--key", scoped_key],
        check=True,
    )

    # Now remove the migrations up to the target.
    subprocess.run(
        [
            "fluctuate",
            "--verbosity",
            "DEBUG",
            "unmigrate",
            "--key",
            scoped_key,
            "--target",
            "cli_test.test_migration_5",
        ],
        check=True,
    )

    # Check that all migrations on the top level database remain.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database is None
            ),
        )
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that the migrations themselves were actually applied.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_1") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_2") != null')
    )
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_1") != null')
    ).data

    # Check that the target migration was unapplied in the child database.
    fauna_v10_test_client = Client(
        secret=_build_scoped_key(key=scoped_key, child_db="test_database_1"),
        max_attempts=10,
    )
    fauna_v4_test_client = FaunaClient(
        secret=_build_scoped_key(key=scoped_key, child_db="test_database_1")
    )
    # Check that all migration documents and schema documents were written as expected.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database == "test_database_1"
                and migration.name != "test_migration_5"
            ),
        )
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that only the target migration was unapplied, but the rest remain.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_3") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_2") == null')
    ).data


@pytest.mark.integration
def test_unmigrate_target_then_all_cli(
    fauna_v10_test_client,
    fauna_v4_test_client,
    test_db,
    test_db_scoped_key,
    fauna_admin_key,
):
    """Test the unimgrate command against a test database and ensure the test migrations
    are unapplied up to the target migration, and then to all migrations after running
    the command a second time.
    """
    scoped_key = test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    # Run the migrate command.
    subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "migrate", "--key", scoped_key],
        check=True,
    )

    # Now remove the migrations up to the target.
    subprocess.run(
        [
            "fluctuate",
            "--verbosity",
            "DEBUG",
            "unmigrate",
            "--key",
            scoped_key,
            "--target",
            "cli_test.test_migration_5",
        ],
        check=True,
    )

    # Check that all migrations on the top level database remain.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database is None
            ),
        )
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that the migrations themselves were actually applied.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_1") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_2") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_1") != null')
    ).data

    # Check that the target migration was unapplied in the child database.
    fauna_v10_test_client = Client(
        secret=_build_scoped_key(key=scoped_key, child_db="test_database_1"),
        max_attempts=10,
    )
    fauna_v4_test_client = FaunaClient(
        secret=_build_scoped_key(key=scoped_key, child_db="test_database_1")
    )
    # Check that all migration documents and schema documents were written as expected.
    assert fauna_v10_test_client.query(
        fql(
            "Collection.byName(${migrations_collection_name}) != null",
            migrations_collection_name=migrations_collection_name,
        )
    ).data
    assert fauna_v10_test_client.query(
        fql(
            f"""
            let migrations = ${{migrations_tuple}}
            migrations.every(
                migration => {{
                    {migrations_collection_name}.by_name_and_namespace(
                        migration.name, migration.namespace
                    ) != null
                }}
            )
            """,
            migrations_tuple=tuple(
                {"name": migration.name, "namespace": migration.namespace}
                for migration in migrations
                if migration.child_database == "test_database_1"
                and migration.name != "test_migration_5"
            ),
        )
    ).data
    assert _run_fql_v4_query_with_retry(
        fauna_v4_client=fauna_v4_test_client,
        query=query.exists(query.index(migrations_index_name)),
    )
    # Test that only the target migration was unapplied, but the rest remain.
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_3") != null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_2") == null')
    ).data

    # Now remove all migrations.
    subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "unmigrate", "--key", scoped_key],
        check=True,
    )

    # Should have removed all of the collections.
    fauna_v10_test_client = Client(secret=scoped_key, max_attempts=10)
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_1") == null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Collection.byName("test_collection_2") == null')
    ).data
    assert fauna_v10_test_client.query(
        fql('Database.byName("test_database_1") == null')
    ).data


@pytest.mark.integration
def test_unmigrate_wrong_target(test_db, test_db_scoped_key, fauna_admin_key):
    """Test that specifying an invalid target for unmigrate causes the command to exit
    with an error.
    """
    scoped_key = test_db_scoped_key(test_db=test_db, admin_key=fauna_admin_key)
    # Run the migrate command and expect a failure.
    with pytest.raises(subprocess.CalledProcessError):
        subprocess.run(
            [
                "fluctuate",
                "--verbosity",
                "DEBUG",
                "unmigrate",
                "--key",
                scoped_key,
                "--target",
                "foo",
            ],
            check=True,
        )


@pytest.mark.integration
def test_version():
    """Test that the --version flag prints the version."""
    result = subprocess.run(
        ["fluctuate", "--verbosity", "DEBUG", "--version"],
        check=True,
        capture_output=True,
        text=True,
    )

    assert version("fluctuate") in result.stderr


@patch(target="fluctuate.cli.get_fauna_secret", autospec=True)
def test_retrieve_common_options(mock_get_fauna_secret):
    """Test that `retrieve_common_options` retrieves the key from the secrets manager if
    the `secret_arn` is specified.
    """
    mock_secret_arn = MagicMock()

    result = _process_common_options(key=None, secret_arn=mock_secret_arn)

    # Should have returned the secret value from the AWS SecretsManager secret.
    assert result == mock_get_fauna_secret.return_value.__getitem__.return_value
    mock_get_fauna_secret.assert_called_once_with(arn=mock_secret_arn)
    mock_get_fauna_secret.return_value.__getitem__.assert_called_once_with("secret")


@patch(target="fluctuate.cli.get_fauna_secret", autospec=True)
def test_retrieve_common_options_no_secret_arn(mock_get_fauna_secret):
    """Test that `retrieve_common_options` returns the key if no secret_arn is provided."""
    mock_key = MagicMock()

    result = _process_common_options(key=mock_key, secret_arn=None)

    # Should have returned the key provided and not done anything else.
    assert result == mock_key
    mock_get_fauna_secret.assert_not_called()
    mock_get_fauna_secret.return_value.__getitem__.assert_not_called()
