import sys
from importlib import import_module, reload
from unittest.mock import MagicMock, call, patch

import pytest

from fluctuate.secrets_manager import DummyModule, get_fauna_secret


@patch(target="fluctuate.secrets_manager.json", autospec=True)
@patch(target="fluctuate.secrets_manager.FaunaDecoder", autospec=True)
@patch(target="fluctuate.secrets_manager.parse_json", autospec=True)
@patch(target="fluctuate.secrets_manager.boto3", autospec=True)
def test_get_fauna_secret_v4(
    mock_boto3, mock_parse_json, mock_fauna_decoder, mock_json
):
    """Test that getting the fauna secret properly parses the FQLv4 result and returns
    it.
    """
    # The fauna decoder would fail to parse a FQLv4 key object, so make it raise.
    mock_fauna_decoder.decode.side_effect = KeyError("col1")
    mock_arn = MagicMock()

    result = get_fauna_secret(arn=mock_arn)

    # Should have returned the parsed FaunaDB Key object.
    assert result == mock_parse_json.return_value
    # Should have retrieved the secret from AWS.
    mock_boto3.client.assert_called_once_with("secretsmanager")
    mock_boto3.client.return_value.get_secret_value.assert_called_once_with(
        SecretId=mock_arn
    )
    mock_boto3.client.return_value.get_secret_value.return_value.__getitem__.assert_has_calls(
        (call("SecretString"), call("SecretString"))
    )
    # Should have attempted to parse the secret into a FaunaDB Key object using FQLv10.
    mock_json.loads.assert_called_once_with(
        mock_boto3.client.return_value.get_secret_value.return_value.__getitem__.return_value
    )
    mock_fauna_decoder.decode.assert_called_once_with(mock_json.loads.return_value)
    # Should have parsed the secret into a FaunaDB Key object using FQLv4.
    mock_parse_json.assert_called_once_with(
        mock_boto3.client.return_value.get_secret_value.return_value.__getitem__.return_value
    )


@patch(target="fluctuate.secrets_manager.json", autospec=True)
@patch(target="fluctuate.secrets_manager.FaunaDecoder", autospec=True)
@patch(target="fluctuate.secrets_manager.parse_json", autospec=True)
@patch(target="fluctuate.secrets_manager.boto3", autospec=True)
def test_get_fauna_secret_v10(
    mock_boto3, mock_parse_json, mock_fauna_decoder, mock_json
):
    """Test that getting the fauna secret properly parses the FQLv10 result and returns
    it.
    """
    mock_arn = MagicMock()

    result = get_fauna_secret(arn=mock_arn)

    # Should have returned the parsed FaunaDB Key object.
    assert result == mock_fauna_decoder.decode.return_value
    # Should have retrieved the secret from AWS.
    mock_boto3.client.assert_called_once_with("secretsmanager")
    mock_boto3.client.return_value.get_secret_value.assert_called_once_with(
        SecretId=mock_arn
    )
    mock_boto3.client.return_value.get_secret_value.return_value.__getitem__.assert_has_calls(
        (call("SecretString"),)
    )
    # Should have parsed the secret into a FaunaDB Key object using FQLv10.
    mock_json.loads.assert_called_once_with(
        mock_boto3.client.return_value.get_secret_value.return_value.__getitem__.return_value
    )
    mock_fauna_decoder.decode.assert_called_once_with(mock_json.loads.return_value)
    # Should not have used FQLv4.
    mock_parse_json.assert_not_called()


@pytest.mark.parametrize(
    "mock_secret_string",
    (
        '{"ref":{"@ref":{"id":"365653800647655489","collection":{"@ref":{"id":"keys"}}}},"ts":1684973488405000,"role":"admin","data":{"name":"v4_repl_test"},"secret":"test_secret","hashed_secret":"$2a$05$DA9gIUUgTFcUrYvlyd/RpeXsFJcC2YX2HLBsOB/YEWLttnTrHa9wm"}',
        '{"id": "365653458158616641", "coll": {"@mod": "Key"}, "ts": {"@time": "2023-05-25T00:06:01.820000+00:00"}, "secret": "test_secret", "role": "admin", "data": {"name": "repl-test"}}',
    ),
)
@patch(target="fluctuate.secrets_manager.boto3", autospec=True)
def test_get_fauna_secret_parsing(mock_boto3, mock_secret_string):
    """Test that getting the fauna secret properly parses the FQLv4 result and returns
    it when actually parsing values.
    """
    mock_arn = MagicMock()
    mock_boto3.client.return_value.get_secret_value.return_value.__getitem__.return_value = (
        mock_secret_string
    )

    result = get_fauna_secret(arn=mock_arn)

    # Should have returned the parsed FaunaDB Key object.
    assert result["secret"] == "test_secret"


def test_dummy_module():
    """Test that the dummy module properly raises an error whenever any attribute is
    accessed.
    """
    test_instance = DummyModule()

    with pytest.raises(ImportError, match=r"aws_secrets_manager"):
        test_instance.client("secretsmanager")


def test_dummy_module_getattr():
    """Test that the dummy module properly raises an error whenever any attribute is
    accessed.
    """
    test_instance = DummyModule()

    with pytest.raises(ImportError, match=r"aws_secrets_manager"):
        getattr(test_instance, "client")


def test_import_error():
    """Test that not installing boto3 will allow the module to be imported, but it will
    raise if any boto3 attribute is accessed.
    """
    with patch.dict(sys.modules):
        sys.modules["boto3"] = None
        if "fluctuate.secrets_manager" in sys.modules:
            reload(sys.modules["fluctuate.secrets_manager"])
        else:
            import_module("fluctuate.secrets_manager")

        with pytest.raises(ImportError, match=r"aws_secrets_manager"):
            get_fauna_secret("foo")
