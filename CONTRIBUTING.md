# Table of contents

[[_TOC_]]

# Install python dependencies

This project uses [poetry] for dependency and environment management. To get started,
you will need to follow the instructions for your system to [install poetry].

This project supports Python 3.8+. You might consider installing [pyenv] if your system
does not ship with one of those versions.

Once poetry is installed, change to the root of the repository and install all
dependencies including development and extra ones:

```bash
poetry install --all-extras
```

To install additional packages, add them via poetry:

```bash
poetry add <package name>
```

To install additional development packages (ones not needed for the lambda itself):

```bash
poetry add --group dev <package name>
```

# Testing locally

These instructions cover how to test locally on your machine. They assume you have
already installed the dependencies.

## Running the tests

To run the test suite locally, run the following command.

```bash
poetry run pytest -m "not integration"
```

You can watch for changes and re-run all tests using the `looponfail` option thanks to
the [pytest-xdist] plugin.

```bash
poetry run pytest -m "not integration" --looponfail
```

To check the coverage metrics, you can use [pytest-cov]

```bash
poetry run pytest -m "not integration" --cov
```

## Integration tests

You may notice that we are using a mark to disable all tests marked as integration. This
is because there are additional configuration steps required in order to run them as
outlined below.

### FaunaDB admin key

To run any tests that actually test against [Fauna DB], you must either set
`FAUNA_ADMIN_KEY` or `FAUNA_ADMIN_KEY_SECRET_ARN` as an environment variable or in a
.env file.

If using `FAUNA_ADMIN_KEY`, it is preferred to set it as an environment variable, as it
has less likelihood of ending up committed to the repository.

If using `FAUNA_ADMIN_KEY_SECRET_ARN`, it requires configuring [boto3 credentials] via
one of the documented methods that does not involve setting the credentials on the
client or session objects. In addition, the credentials used must have permissions to
call [getSecretValue] on the secret.

### Secrets Manager credentials

In order to talk to [Secrets Manager], [boto3 credentials] must be set via one of the
documented methods that does not involve setting the credentials on the client or
session objects.

In addition, the credentials used must have permission to call:

* [createSecret]
* [deleteSecret]
* [describeSecret]

### Running integration tests

Now that all the prerequisites are satisfied, the integration tests can be run by not
passing the mark expression:

```bash
poetry run pytest
```

# Commit format

This repository follows the [conventional commits] specification for commit messages.
This is to enable usage of [semantic release] to automatically generate releases,
changelogs, release notes, etc.

# Git hooks

This repository uses [pre-commit] in order to manage a set of hooks that do things like:

* ensure code is linted and has no errors
* ensure consistent formatting and styling

After you have [installed the dependencies] you can set up the git hooks by running:

```bash
poetry run pre-commit install
```

Now when you commit, it will automatically run the repo hooks and reject the commit if
something is wrong. Most hooks will automatically fix the problems for you, so in most
cases you will simply need to check the changes they made, add them, and commit again.

[poetry]: https://python-poetry.org/docs/
[install poetry]: https://python-poetry.org/docs/#installation
[pyenv]: https://github.com/pyenv/pyenv
[pytest-xdist]: https://pytest-xdist.readthedocs.io/en/latest/
[pytest-cov]: https://pytest-cov.readthedocs.io/en/latest/
[Fauna DB]: https://docs.fauna.com/fauna/current/
[boto3 credentials]: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
[getSecretValue]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/secretsmanager.html#SecretsManager.Client.get_secret_value
[Secrets Manager]: https://docs.aws.amazon.com/secretsmanager/index.html
[createSecret]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/secretsmanager.html#SecretsManager.Client.create_secret
[deleteSecret]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/secretsmanager.html#SecretsManager.Client.delete_secret
[describeSecret]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/secretsmanager.html#SecretsManager.Client.describe_secret
[conventional commits]: https://www.conventionalcommits.org/en/v1.0.0/
[semantic release]: https://semantic-release.gitbook.io/semantic-release/
[pre-commit]: https://pre-commit.com/
[installed the dependencies]: #install-python-dependencies
